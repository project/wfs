
# WFS Module

The WFS module makes a View into a very simple WFS provider. Its main 
intended use is for Drupal-hosted data to be renderable by GeoServer 
to avoid complications of presenting large numbers of dots on a map 
or complex data on a map.

## Requirements

* Views

## Compatible Modules

* [OpenLayers](http://drupal.org/project/openlayers)

## Compatible Software

* Desktop GIS: [QGIS](http://www.qgis.org/) w/ WFS Plugin
* Web GIS: [GeoServer](http://geoserver.org/) (not working yet)

## Credits

* [tmcw](http://drupal.org/user/12664)
