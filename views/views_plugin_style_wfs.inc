<?php

/**
 * @file
 * Extending the view_plugin_style class to provide a wfs view style.
 */
class views_plugin_style_wfs extends views_plugin_style {

  /**
   * Initialize plugin.
   *
   * Set feed image for shared rendering later.
   */
  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options = NULL);
    // TODO: get better WFS icon
    $this->feed_image = drupal_get_path('module', 'views_wfs') . 
      '/images/wfs.png';
  }

  /**
   * Attach this view to another display as a feed.
   *
   * Provide basic functionality for all export style views like attaching a
   * feed image link.
   */
  function attach_to($display_id, $path, $title) {
    $url_options = array('html' => TRUE);
    $image = theme('image', $this->feed_image);
    $this->view->feed_icon .= l($image, $path, $url_options);
  }

  /**
   * Provide a form for setting options.
   *
   * @param array $form
   * @param array $form_state
   */
  function options_form(&$form, &$form_state) { 
    parent::options_form($form, $form_state);                  
    $options = parent::option_definition();

    $handlers = $this->display->handler->get_handlers('field');
    if (empty($handlers)) {
      $form['error_markup'] = array(
        '#value' => t('You need at least one field before you can 
        configure your field settings'),
        '#prefix' => '<div class="error form-item description">',
        '#suffix' => '</div>',
      );
    }
    else {
      $field_names[$field] = array('' => '--');
      foreach ($handlers as $field => $handler) {
        if ($label = $handler->label()) {
          $field_names[$field] = $label;
        }
        else {
          $field_names[$field] = $handler->ui_name();
        }
      }
      $field_options = array(
        'longitude' => t('Longitude'),
        'latitude' => t('Latitude'),
        'id' => t('ID'),
      );
      $form['fields'] = array(
        '#type' => 'fieldset',
        '#title' => 'Field usage',
        '#description' => t('Select the fields that contain the latitude,
        longitude and id of each point. 
        Remaining fields will be in the attributes of the point.'),
        '#weight' => -10,
      );
      foreach ($field_options as $k => $v) {
        $form['fields'][$k] = array(
          '#type' => 'select',
          '#title' => $v,
          '#options' => $field_names,
          '#default_value' => $this->options['fields'][$k],
          '#required' => ($k == 'class' ? FALSE : TRUE),
        );
      }
    }
  }

  function field_names($rows) {
    return array_keys($this->view->field);
  }

  /**
   * @param $rows the rows of a rendered view
   * @return $points all of the rows in that view which formed
   *  valid coordinates, organized into coordinates and attributes
   */
  function map_rows($rows) {
    // Fields must be rendered in order as of Views 2.3, 
    // so we will pre-render everything.
    $renders = array();
    $keys = array_keys($this->view->field);

    foreach ($rows as $count => $row) {
      foreach ($keys as $id) {
        $renders[$count][$id] = $this->view->field[$id]->theme($row);
      }
    }  

    $points = array();
    foreach ($renders as $id => $row) {
      $point = array();
      $point['attr'] = array();
      foreach ($this->view->field as $key => $field) {
        if ($key == $this->options['fields']['longitude']) {
          $point['lon'] = $row[$key];
        }
        elseif ($key == $this->options['fields']['latitude']) {
          $point['lat'] = $row[$key];
        }
        elseif ($key == $this->options['fields']['id']) {
          $point['id'] = $row[$key];
        }
        $point['attr'][$key] = $row[$key];
      }

      // TODO: this could be expressed more succinctly
      if(isset($point['lat'], $point['lon']) && $point['lat'] != '' && $point['lon'] != '') {
        $point['point'] = $point['lon'] . ' ' . $point['lat'];
        unset($point['lat']);
        unset($point['lon']);
        $points[] = $point;
      }
    }
    return $points;
  }
}
