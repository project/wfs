<?php

/**
 * @file
 * Theming functions for KML module views output.
 */

function template_preprocess_views_view_wfs(&$vars) {
  $view = $vars['view'];

  $view_url = url(
    $view->display[$view->current_display]->display_options['path'],
    array('absolute' => TRUE));

  watchdog('warning', request_uri());

  // case of parameters is not in the WFS spec
  $request = isset($_GET['request']) ? 
    $_GET['request'] : $_GET['REQUEST'];

  if ($request) {
    switch ($request) { 
      case 'GetCapabilities':
        drupal_set_header('Content-Type: text/xml');

        // TODO: discern correctly
        echo theme('wfs_getcapabilities', 
          $view_url, 
          $view->name, 
          $view->description,
          variable_get('site_name', ''));
        exit();

      case 'DescribeFeatureType':
        drupal_set_header('Content-Type: text/xml; subtype=gml/3.1.1');
        echo theme('wfs_describefeaturetype',
          $view->name, 
          $view->style_plugin->field_names($vars['rows']));
        exit();
    }
  }

  $points = $view->style_plugin->map_rows($vars['rows']);

  foreach ($points as $point) {
    $rows .= theme('wfs_placemark', $point, $view->name);
  }

  $vars['rows'] = $rows;
  $vars['style'] = theme('wfs_style', $row);
  $vars['viewtitle'] = $view->name;  

  drupal_set_header('Content-Type: text/xml');
}

/**
 * Preprocess for theme('wfs_placemark').
 *
 * @param $vars a single placemark array
 */
function template_preprocess_wfs_placemark(&$vars) {  
  // $vars['name'] = filter_xss_admin($vars['point']['name']);
  // $vars['description'] = filter_xss_admin($vars['point']['description']);
  $vars['coords'] = check_plain($vars['point']['point']);
  $vars['attr'] = $vars['point']['attr'];
  $vars['feature_id'] = $vars['point']['id'];
}

/**
 * Theme function for wfs feed icon.
 *
 * @param $url URL of the WFS Feed
 * @param $title Title of the image element
 * @param $icon Icon URL
 *
 * @return string Nothing, currently. WFS is not user-consumable
 */
function theme_wfs_feed_icon($url, $title, $icon) {
  return '';
}
